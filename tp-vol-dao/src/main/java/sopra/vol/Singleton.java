package sopra.vol;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import sopra.vol.dao.IAeroportDao;
import sopra.vol.dao.IClientDao;
import sopra.vol.dao.ICompagnieDao;
import sopra.vol.dao.IPassagerDao;
import sopra.vol.dao.IReservationDao;
import sopra.vol.dao.IVilleDao;
import sopra.vol.dao.IVolDao;
import sopra.vol.dao.IVoyageDao;
import sopra.vol.dao.IVoyageVolDao;
import sopra.vol.dao.jpql.AeroportDao;
import sopra.vol.dao.jpql.ClientDao;
import sopra.vol.dao.jpql.CompagnieDao;
import sopra.vol.dao.jpql.PassagerDao;
import sopra.vol.dao.jpql.ReservationDao;
import sopra.vol.dao.jpql.VilleDao;
import sopra.vol.dao.jpql.VolDao;
import sopra.vol.dao.jpql.VoyageDao;
import sopra.vol.dao.jpql.VoyageVolDao;



public class Singleton {
	private static Singleton instance = null;
	
	private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("volJpql");
	
	private final IVilleDao villeDao = new VilleDao();
	private final IAeroportDao aeroDao = new AeroportDao();
	private final IVolDao volDao = new VolDao();
	private final ICompagnieDao compagnieDao = new CompagnieDao();
	private final IVoyageVolDao voyageVolDao = new VoyageVolDao();
	private final IVoyageDao voyageDao = new VoyageDao();
	private final IReservationDao reservationDao = new ReservationDao();
	private final IPassagerDao passagerDao = new PassagerDao();

	private final IClientDao clientDao = new ClientDao();
	
	public static Singleton getInstance() {
		if (instance == null) {
			instance = new Singleton();
		}

		return instance;
	}
	
	public EntityManagerFactory getEmf() {
		return emf;
	}
	
	public IVilleDao getVilleDao() {
		return villeDao;
	}
	
	public IAeroportDao getAeroportDao() {
		return aeroDao;
	}
	
	public IVolDao getVolDao() {
		return volDao;
	}
	
	public ICompagnieDao getCompagnieDao() {
		return compagnieDao;
	}
	
	public IVoyageVolDao getVoyageVolDao() {
		return voyageVolDao;
	}
	
	public IVoyageDao getVoyageDao() {
		return voyageDao;
	}
	
	public IReservationDao getReservationDao() {
		return reservationDao;
	}

	public IClientDao getClientDao() {
		return clientDao;
	}
	
	public IPassagerDao getPassagerDao() {
		return passagerDao;
	}
}
