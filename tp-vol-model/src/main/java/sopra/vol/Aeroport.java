package sopra.vol;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "aeroport")
public class Aeroport {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "code_aeroport", length = 100, nullable = true)
	private String code;

	@ManyToMany(mappedBy = "aeroports")
	private List<Ville> villes = new ArrayList<Ville>();
	
	@OneToMany(mappedBy = "depart")
	private List<Vol> departs = new ArrayList<Vol>();
	
	@OneToMany(mappedBy = "arrivee")
	private List<Vol> arrivees = new ArrayList<Vol>();

	public Aeroport() {
		super();
	}

	public Aeroport(String code) {
		super();
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<Ville> getVilles() {
		return villes;
	}

	public void setVilles(List<Ville> villes) {
		this.villes = villes;
	}

	public List<Vol> getDeparts() {
		return departs;
	}

	public void setDeparts(List<Vol> departs) {
		this.departs = departs;
	}

	public List<Vol> getArrivees() {
		return arrivees;
	}

	public void setArrivees(List<Vol> arrivees) {
		this.arrivees = arrivees;
	}
	
}
