package sopra.vol;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="voyagevol")
public class VoyageVol {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "ordre_vols")
	private int ordre;

	@ManyToOne
	@JoinColumn(name = "voyage_vol_id")
	private Voyage voyagevo;
	
	@ManyToOne
	@JoinColumn(name = "vol_id")
	private Vol vol;

	public VoyageVol() {
		super();
	}

	public VoyageVol(Long id, int ordre) {
		super();
		this.id = id;
		this.ordre = ordre;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getOrdre() {
		return ordre;
	}

	public void setOrdre(int ordre) {
		this.ordre = ordre;
	}

	public Voyage getVoyage() {
		return voyagevo;
	}

	public void setVoyage(Voyage voyage) {
		this.voyagevo = voyage;
	}

	public Vol getVol() {
		return vol;
	}

	public void setVol(Vol vol) {
		this.vol = vol;
	}

	
}
