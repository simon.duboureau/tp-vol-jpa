package sopra.vol.dao;

import sopra.vol.Compagnie;

public interface ICompagnieDao extends IDao<Compagnie, Long>{
}
