package sopra.vol;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import sopra.vol.dao.IClientDao;
import sopra.vol.dao.IPassagerDao;
import sopra.vol.dao.IReservationDao;

import sopra.vol.dao.IAeroportDao;
import sopra.vol.dao.ICompagnieDao;
import sopra.vol.dao.IVilleDao;
import sopra.vol.dao.IVolDao;
import sopra.vol.dao.IVoyageDao;
import sopra.vol.dao.IVoyageVolDao;

public class TestTables {

	public static void main(String[] args) {

		IClientDao clientDao = Singleton.getInstance().getClientDao();
		IPassagerDao passagerDao = Singleton.getInstance().getPassagerDao();
		IReservationDao reservationDao = Singleton.getInstance().getReservationDao();



		ClientParticulier clt_dubois = null;
		ClientPro atlanticSecretariat = null;
		Adresse adresseDubois = null;
		Reservation premiereResa = null;
		Reservation deuxiemeResa = null;
		Passager jeanPassager = null;



			clt_dubois = new ClientParticulier();
			clt_dubois.setCivilite(Civilite.M);
			clt_dubois.setNom("DUBOIS");
			clt_dubois.setPrenom("Jean");
			clt_dubois.setMail("dubois_j@gmail.com");
			clt_dubois.setMoyenPaiement(MoyenPaiement.ESPECE);
			clientDao.save(clt_dubois);

			atlanticSecretariat = new ClientPro();
			atlanticSecretariat.setMail("as@gmail.com");
			atlanticSecretariat.setMoyenPaiement(MoyenPaiement.VIREMENT);
			atlanticSecretariat.setTypeEntreprise(TypeEntreprise.SA);
			atlanticSecretariat.setNomEntreprise("Atlantic Secretariat");
			atlanticSecretariat.setNumeroSiret("5651fgbdf");
			atlanticSecretariat.setNumeroSiret("2145fb");
			atlanticSecretariat.setNumTVA("11513k");
			atlanticSecretariat.setTelephone("0645875963");
			clientDao.save(atlanticSecretariat);


			adresseDubois = new Adresse();
			adresseDubois.setVoie("5 avenie de candau");
			adresseDubois.setCodePostal("33600");
			adresseDubois.setPays("France");
			adresseDubois.setVille("Pessac");
			clt_dubois.setPrincipale(adresseDubois);

			premiereResa = new Reservation();
			premiereResa.setClient(clt_dubois);
			premiereResa.setTarif(1500f);
			premiereResa.setNumero("1547F");

			
			deuxiemeResa = new Reservation();
			deuxiemeResa.setClient(clt_dubois);
			deuxiemeResa.setTarif(1700f);
			deuxiemeResa.setNumero("1548F");

			jeanPassager = new Passager();
			jeanPassager.setCivilite(Civilite.M);
			jeanPassager.setNationalite("Française");
			jeanPassager.setTypePI(TypePieceIdentite.PASSEPORT);
			jeanPassager.setNom("DUBOIS");
			jeanPassager.setPrenom("Jean");
			jeanPassager.setPrincipale(adresseDubois);
			premiereResa.setPassager(jeanPassager);
			passagerDao.save(jeanPassager);


			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			try {
				jeanPassager.setDtNaissance(sdf.parse("20-08-1960"));
			} catch (ParseException e) {
				e.printStackTrace();

				System.out.println(clt_dubois.getPrenom()+" a "+ premiereResa.getNumero()+" numero de  reservation");
			}
		IVilleDao villeDao = Singleton.getInstance().getVilleDao();

		Ville bdx = new Ville();
		Ville paris = new Ville();
		Ville ny = new Ville();

		bdx.setNom("Bordeaux");
		paris.setNom("Paris");
		ny.setNom("New York");

		villeDao.save(bdx);
		villeDao.save(paris);
		villeDao.save(ny);
		
		IAeroportDao aeroportDao = Singleton.getInstance().getAeroportDao();

		Aeroport bdxarp = new Aeroport();
		Aeroport parisarp = new Aeroport();
		Aeroport nyarp = new Aeroport();
		bdxarp.setCode("MLK235");
		parisarp.setCode("MMF855");
		nyarp.setCode("DFR234");
		
		aeroportDao.save(bdxarp);
		aeroportDao.save(parisarp);
		aeroportDao.save(nyarp);

		ICompagnieDao compagnieDao = Singleton.getInstance().getCompagnieDao();

		Compagnie bdxcomp = new Compagnie();
		bdxcomp.setNomCompagnie("EasyJet");
		Compagnie pariscomp = new Compagnie();
		pariscomp.setNomCompagnie("RyanAir");
		
		compagnieDao.save(bdxcomp);
		compagnieDao.save(pariscomp);

		IVolDao volDao = Singleton.getInstance().getVolDao();

		Vol vol1 = new Vol();
		vol1.setNumero("325698FFG");
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
		try {
			vol1.setDateDepart(sdf1.parse("22-09-2019"));
			vol1.setDateArrivee(sdf1.parse("03-10-2019"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		vol1.setOuvert(true);
		vol1.setNbPlaces(400);
		vol1.setDepart(bdxarp);
		vol1.setArrivee(parisarp);
		vol1.setCompagnie(bdxcomp);

			List <Reservation> resaClientDubois = new ArrayList<Reservation>();
			resaClientDubois.add(premiereResa);
			resaClientDubois.add(deuxiemeResa);
//			reservationDao.save(resaClientDubois);
			
		Vol vol2 = new Vol();
		vol2.setNumero("323678FGL");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
		try {
			vol2.setDateDepart(sdf2.parse("03-10-2019"));
			vol2.setDateArrivee(sdf2.parse("13-10-2019"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		vol2.setOuvert(true);
		vol2.setNbPlaces(400);
		vol2.setDepart(parisarp);
		vol2.setArrivee(nyarp);
		vol2.setCompagnie(pariscomp);
		
		volDao.save(vol1);
		volDao.save(vol2);
		
		
		IVoyageDao voyageDao = Singleton.getInstance().getVoyageDao();
		IVoyageVolDao voyagevolDao = Singleton.getInstance().getVoyageVolDao();

		VoyageVol bdxparis = new VoyageVol();
		VoyageVol parisny = new VoyageVol();
		bdxparis.setVol(vol1);
		bdxparis.setOrdre(1);
		parisny.setVol(vol2);
		parisny.setOrdre(2);
		
		List<VoyageVol> voyages = new ArrayList<VoyageVol>();
		voyages.add(bdxparis);
		voyages.add(parisny);
		
		Voyage voyage1 = new Voyage();
		
		voyagevolDao.save(bdxparis);
		voyagevolDao.save(parisny);
		
		voyage1.setVols(voyages);

		voyageDao.save(voyage1);
	}
}

