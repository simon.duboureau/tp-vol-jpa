package sopra.vol.dao;

import sopra.vol.Voyage;

public interface IVoyageDao extends IDao<Voyage, Long> {
}
