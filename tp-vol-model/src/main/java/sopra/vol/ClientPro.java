package sopra.vol;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@DiscriminatorValue("Client_pro")
public class ClientPro extends Client {
	@Column(name = "numeroSiret", nullable = true, length = 100)
	private String numeroSiret;
	@Column(name = "nomEntreprise", nullable = true, length = 100)
	private String nomEntreprise;
	@Column(name = "numTVA", nullable = true, length = 50)
	private String numTVA;
	@Enumerated(EnumType.STRING)
	@Column(name = "typeEntreprise", nullable = true, length = 50)
	private TypeEntreprise typeEntreprise;

	public ClientPro() {
		super();
	}

	public ClientPro(Long id, String mail, String telephone, MoyenPaiement moyenPaiement, String numeroSiret,
			String nomEntreprise, String numTVA, TypeEntreprise typeEntreprise) {
		super(id, mail, telephone, moyenPaiement);
		this.numeroSiret = numeroSiret;
		this.nomEntreprise = nomEntreprise;
		this.numTVA = numTVA;
		this.typeEntreprise = typeEntreprise;
	}

	public String getNumeroSiret() {
		return numeroSiret;
	}

	public void setNumeroSiret(String numeroSiret) {
		this.numeroSiret = numeroSiret;
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}

	public String getNumTVA() {
		return numTVA;
	}

	public void setNumTVA(String numTVA) {
		this.numTVA = numTVA;
	}

	public TypeEntreprise getTypeEntreprise() {
		return typeEntreprise;
	}

	public void setTypeEntreprise(TypeEntreprise typeEntreprise) {
		this.typeEntreprise = typeEntreprise;
	}

}
