package tp.vol.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class TestClients {
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("volJpql");
		EntityManager em = null;
		EntityTransaction tx = null;

		try{
			em = emf.createEntityManager();
			tx = em.getTransaction();
			tx.begin();
			tx.commit(); // em.flush();
		}
		catch (Exception e){
			if (tx != null && tx.isActive()){
				tx.rollback();
			}
			e.printStackTrace();
		}finally {
			em.close();
		}
		emf.close();
	}
}

